import React from "react";
import GifList from "./GifList";
import NavBar from './NavBar';


class GifContainer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            gifs: [],
            search: ""
        }

    }

    getGifs() {
        const URL = `http://api.giphy.com/v1/gifs/search?q=` +
            `${this.state.search}` +
            `&api_key=dc6zaTOxFJmzC&rating=g` 
        fetch(URL)
            .then(response => response.json())
            .then(obj => {
                this.setState({
                    gifs: obj.data
                })
            });
    }

    onSearch(search) {
        this.setState({
            search: search
        }, () => this.getGifs())
    }

    componentDidMount() {
        this.getGifs()
    }

    render() {
        return (
            <div>
                <NavBar title="Giphy Search" onSearch={this.onSearch.bind(this)} />
                <GifList gifs={this.state.gifs} />
            </div>
        )
    }
}

export default GifContainer