import React from 'react';
import GifContainer from "./GifContainer.js";

export default class App extends React.Component {
  render() {
    return (<div>
      <GifContainer />
    </div>);
  }
}
